This project collects pointers to some
[HTML presentations](https://lechten.gitlab.io/talks/) generated with
[emacs-reveal](https://gitlab.com/oer/emacs-reveal).

Links to source code for presentations:
- [Talks in 2017](https://gitlab.com/lechten/talks-2017)
- [Talks in 2018](https://gitlab.com/lechten/talks-2018)
