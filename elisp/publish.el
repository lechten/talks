;;; publish.el --- Publish index file for presentations on Gitlab Pages
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; SPDX-FileCopyrightText: 2018-2021 Jens Lechtenbörger
;; SPDX-License-Identifier: GPL-3.0-or-later

;;; Commentary:
;; Originally inspired by publish.el by Rasmus:
;; https://gitlab.com/pages/org-mode/blob/master/publish.el


;;; Code:
;; Avoid update of emacs-reveal, enable stacktraces.
(setq emacs-reveal-managed-install-p nil
      debug-on-error t)

;; Set up load-path.
(let ((install-dir
       (mapconcat #'file-name-as-directory
                  `(,user-emacs-directory "elpa" "emacs-reveal") "")))
  (add-to-list 'load-path install-dir))
(require 'emacs-reveal)

(oer-reveal-publish-all
 (list
  (list "redirect"
	:base-directory "redirect"
	:base-extension 'any
	:publishing-directory "./public"
	:publishing-function 'org-publish-attachment)
  ))
;;; publish.el ends here
