# Local IspellDict: en
#+SPDX-FileCopyrightText: 2017-2021,2023 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../reveal.js/dist/theme/index.css" />
#+STARTUP: showeverything
#+TITLE: Selected OER presentations created with emacs-reveal
#+AUTHOR: Jens Lechtenbörger
#+KEYWORDS: open educational resources, OER, presentation, creative commons, emacs-reveal
#+DESCRIPTION: Selected OER presentations under Creative Commons license CC BY-SA by Jens Lechtenbörger created with emacs-reveal

#+OPTIONS: html-style:nil
#+OPTIONS: toc:nil num:nil
#+OER_REVEAL_RDF_TYPEOF: nil

All presentations listed here are
[[https://en.wikipedia.org/wiki/Open_educational_resources][Open Educational Resources (OER)]]
under the Creative Commons license [[https://creativecommons.org/licenses/by-sa/4.0/][CC BY-SA 4.0]]
that have been created with the
[[https://en.wikipedia.org/wiki/Free_and_open-source_software][free/libre and open source]]
(FLOSS) software
[[https://gitlab.com/oer/emacs-reveal][emacs-reveal]].
Source files for presentations are available on GitLab.  The URL for
each presentation indicates where to look for the project:  If the
domain name starts with ~oer~, the source project belongs to the
[[https://gitlab.com/oer][group ~oer~]], and the first part of the
URL’s path indicates the project’s name.  Alternatively, source
projects for domains starting with ~lechten~ are
[[https://gitlab.com/lechten][personal projects]]; again, paths
indicate projects’ names.

* Presentations for self-study
  - [[https://oer.gitlab.io/emacs-reveal-howto/howto.html][Emacs-reveal howto]]
  - [[https://oer.gitlab.io/OS/][OER course on Operating Systems]]

* Selected talks
  - 2017-11-29,
    [[https://open-educational-resources.de/veranstaltungen/17/fachforum/][OER Fachforum, Berlin]]
    - [[../talks-2017/2017-oer-fachforum.html][Weiterentwicklung und Erstellung von OER im Selbstversuch]]
      - ([[../talks-2017/2017-oer-fachforum-lightning.html][Kurzversion für Lightning Talk]])
    - [[../talks-2017/2017-oer-emacs-reveal.html][Emacs-reveal – Erzeugung von HTML-Präsentationen mit Audio aus einfachem Textformat]]
  - 2017-12-05, ERCIS Lunchtime Seminar: [[../talks-2017/2017-12-05-JiTT@OS.html][JiTT@OS: Why and how not to lecture]]
  - 2018-04-24: ERCIS Disrupts: Blockchain:
    [[../talks-2018/2018-04-24-Blockchain.html][Blockchain, Consensus, and Databases]]
  - 2019-01-24: [[../talks-2019/2019-01-24-JiTT.html][JiTT@OperatingSystems]]
  - 2019-09-19, [[http://delfi2019.de/][DELFI 2019]]: [[../talks-2019b/2019-09-19-DELFI.html][Simplifying license attribution for OER with emacs-reveal]]
  - 2019-10-15: ERCIS Lunchtime Seminar: [[../talks-2019b/2019-10-15-flipping.html][Lessons Learned from the Flipped Classroom Concept]]
  - 2019-10-22: ERCIS Lunchtime Seminar:
    [[../talks-2019b/2019-10-22-OER.html][Open Educational Resources: What, why, and how?]]
    - [[https://electures.uni-muenster.de/engage/theodul/ui/core.html?id=bfd84252-634d-40d0-996a-3979a21abe3e][Recording of the talk “Open Educational Resources: What, why, and how?”]]
  - 2020-05-05: [[https://wiki.dnb.de/display/DINIAGKIM/KIM+Workshop+2020][KIM 2020]] Lightning Talk:
    [[../talks-2020/2020-05-05-OER.html][Metadaten in und über OER]]
  - 2021-04-21: [[https://oer21.oerconf.org/][OERxDomains 2021]]:
    [[../talks-2021/2021-04-21-OERxDomains.html][Infrastructure and lightweight markup language for OER: The case of emacs-reveal]]
  - 2023-04-27:
    [[https://festival.hfd.digital/de/universityfuture-festival/][University:Future Festival 2023]]:
    [[../talks-2023/2023-04-27-UFF.html][OER on the web - a call to action]]
